'use strict';

var express = require('express')
  , app = express()
  , conf = require('./conf')
  , utils = require('./utils')
  , stylus = require('stylus')
  , nib = require('nib')
  , I18n = require('i18n-2')
  , moment = require('moment')
  , _ = require('underscore');

// Logger
app.configure('development', function() {
  app.use(express.logger('dev'));
});

// Views configuration
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.locals.basedir = __dirname + '/views';

// Parsing JSON, url-encoded and multipart forms
app.use(express.bodyParser());

// _method support
app.use(express.methodOverride());

// Signed cookie parser & session support
app.use(express.cookieParser('bionicman'));
app.use(express.session({
  key: 'sid',
  secret: 'bionicman'
}));

// I18n
I18n.expressBind(app, {
  locales: ['ru'],
  extension: ".json"
});

// Notices
app.use(require('./notices'));

// Configure view locals
app.use(function(req, res, next) {
  req.url = req.url.replace(/\/$/, "");
  _.extend(res.locals, utils, {
    _: _,
    conf: conf,
    path: req.path,
    xhr: req.xhr,
    siteTitle: req.i18n.__('site.title'),
    moment: function(input) {
      return moment(input).lang(req.i18n.getLocale());
    },
    duration: function(args) {
      return moment.duration.call(moment, arguments).lang(req.i18n.getLocale());
    }
  });
  next();
});

app.use(app.router);

app.configure("development", function() {

  // Pretty HTML from Jade
  app.locals.pretty = true;

  // Verbose error handler
  app.use(express.errorHandler({
    dumpExceptions: true,
    showStack: true
  }));

  // Stylus compiler
  app.use(stylus.middleware({
    src: conf.publicPath,
    compile: function(str, path) {
      return stylus(str)
        .set('filename', path)
        .set('compress', true)
        .use(nib());
    }
  }));

  // Public serving with CORS

  app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', conf.origin);
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
  });

  app.use(express.static(conf.publicPath));
});

app.configure('production', function() {
  app.use(express.errorHandler());
});

// Exports

exports = module.exports = app;