"use strict";

// Form validation

$.scalpel.queue['form.validate'] = function() {
  var form = $(this);
  form.validate({
    onsubmit: false
  });
  form[0].handlers.push(function(data) {
    return form.valid();
  });
};

// Sorted tables

$.scalpel.queue['table.sorted'] = function() {

  var headers = {};
  var sortList = [];

  $("thead th", this).each(function() {
    var th = $(this);
    var i = th.index();
    if (!th.hasClass("sorted"))
      headers[i] = { sorter: false };
    if (th.hasClass("asc"))
      sortList.push([i, 0]);
    else if (th.hasClass("desc"))
      sortList.push([i, 1]);
  });

  $(this).tablesorter({
    cssAsc: "desc",
    cssDesc: "asc",
    sortList: sortList,
    headers: headers
  });

};

// Textarea autosize

$.scalpel.queue['textarea.autosize'] = function() {
  $(this).autosize();
};

// MathJAX support

if (typeof MathJax == "undefined") {
  $(window).bind("viewportLoad", function() {
    MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
  });
}

// Code highlighting

if (typeof hljs != "undefined")
  $.scalpel.queue['pre.hl'] = function() {
    hljs.highlightBlock(this);
  };

// Sortables

$.scalpel.queue['.sortable'] = function() {

  var cnt = $(this);

  cnt.sortable({
    items: "> .elem",
    handle: cnt.attr("data-handle") || '.handle',
    placeholder: cnt.attr("data-placeholder") || undefined
  });

  if (cnt.hasClass("submit"))
    cnt.unbind(".sortSubmit").bind("sortupdate.sortSubmit", function() {
      cnt.parents("form").submit();
    });

};

// Load remote markup

$.scalpel.queue[':input[data-load-into][data-url]'] = function() {
  var input = $(this);
  var out = $(input.attr("data-load-into"));
  var t, text = input.val();
  var url = input.attr("data-url");

  function scheduleUpdates() {
    if (text == input.val()) return;
    if (t) clearTimeout(t);
    text = input.val();
    out.addClass("process");
    t = setTimeout(loadUpdates, 500);
  }

  function loadUpdates() {
    $.ajax({
      type: 'post',
      url: url,
      data: { text: input.val() },
      dataType: 'html',
      success: function(data) {
        out.empty().append(data);
        out.removeClass("process");
        if (typeof MathJax != "undefined")
          MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
        $.scalpel.init(out);
      }
    });
  }

  input.unbind(".remoteLoad")
    .bind("keyup.remoteLoad", scheduleUpdates);
};

// Push input value into containers

$.scalpel.queue[':input[data-push-into]'] = function() {
  var input = $(this);
  var cnt = $(input.attr("data-push-into"));

  input.unbind(".pushInto")
    .bind("keyup.pushInto", function() {
      cnt.text(input.val());
    });
};