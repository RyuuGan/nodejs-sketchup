'use strict';

var path = require('path');

exports.env = process.env.NODE_ENV || 'development';
exports.port = process.env.PORT || 3000;
exports.domain = process.env.DOMAIN || 'localhost:3000';
exports.schema = process.env.SSL ? 'https' : 'http';
exports.origin = exports.schema + "://" + exports.domain;
exports.publicPath = __dirname + '/public';

exports.cdnDomain = exports.domain;
exports.cdnOrigin = exports.origin;

exports.storagePath = path.join(
  process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE,
  ".scissors");



